import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");

const STANDARD = { label: 'Standard', factor: 0.05 }
const PREMIUM = { label: 'Premium', factor: 0.1 }
const DIAMOND = { label: 'Diamond', factor: 0.2 }
const NONSENSE = { label: 'Nonsense', factor: 0 }

const TEST_VALUES = [
    {factor: 1, value: 5000},
    {factor: 1.5, value: 10000},
    {factor: 1.5, value: 25000},
    {factor: 2, value: 50000},
    {factor: 2, value: 75000},
    {factor: 2.5, value: 100000},
    {factor: 2.5, value: 200000}
    ]

describe('Bonus tests', () => {
    test('test STANDARD',  (done) => {
        for(const value of TEST_VALUES){
            assert.equal(calculateBonuses(STANDARD.label, value.value), STANDARD.factor * value.factor);
        }
        done();
    });

    test('test PREMIUM',  (done) => {
        for(const value of TEST_VALUES){
            assert.equal(calculateBonuses(PREMIUM.label, value.value), PREMIUM.factor * value.factor);
        }
        done();
    });

    test('test DIAMOND',  (done) => {
        for(const value of TEST_VALUES){
            assert.equal(calculateBonuses(DIAMOND.label, value.value), DIAMOND.factor * value.factor);
        }
        done();
    });

    test('test NONSENSE',  (done) => {
        for(const value of TEST_VALUES){
            assert.equal(calculateBonuses(NONSENSE.label, value.value), NONSENSE.factor * value.factor);
        }
        done();
    });

});